class Post < ActiveRecord::Base
has_many :comments, dependent: :destroy
belongs_to :user
validates_length_of :ttle, :in=>5..30,:message=>"Titulo Invalido"
validates :body, length: {
    minimum: 20,
    maximum: 50,
    tokenizer: lambda { |str| str.split(/\s+/) },
    too_short: "El body tiene muy pocas palabras",
    too_long: "El body tiene demaciadas palabras"
  }



end
